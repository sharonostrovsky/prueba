COMANDOS


Git Init
Para iniciar el trackeo de los archivos de una carpeta.
iniciar repositorio de forma local

Git Status
Nos da toda la información necesaria sobre la rama actual.
nos muestra en que estado se encuentran nuestros archivos
ver los cambios producidos

git remote add origin <URL>

Git Add
Para incluir los cambios del o de los archivos en tu siguiente commit.
git add nombre_archivo
git add .   -> añade todos los archivos

Git Commit
Es como establecer un punto de control en el proceso de desarrollo al cual puedes volver
más tarde si es necesario. Es como un punto de guardado en un videojuego.
guarda los archivos en el repositorio local
git commit -m "mensaje"

Git Push
Enviar archivos incluidos en el commit al repositorio local.

Git Push Origin
Enviar archivos incluidos en el commit al repositorio remoto.

Git Pull
recupera (git fetch) las nuevas confirmaciones y las fusiona(git merge) en tu rama local
actualizar la version local de un repositorio desde otro remoto

git clone <url> .
clonar repositorio en la carpeta donde estoy parado

git pull origin nombre_rama
descargar rama de remoto

git diff
muestra la diferencia entre una version y otra

git log
muestra historial de commit

git config -l  
ver configuracion

git --help

git checkout -b nombre_branch 
crear una rama y cambiarnos a ella

git checkout nombre_branch
pasar de una rama a otra

git show-branch
muestra todas las ramas del proyecto con sus commits realizados

Git Branch
ver en que rama nos encontramos y ver listado de ramas que tengamos en un proyecto

git branch -a 
muestra todas las ramas existentes (local y remoto)

git push origin nombre_branch
subir una rama al repositorio remoto

git merge ramaGit -m "mensaje" 
fusiono la ramaGit con la rama en la que estoy parado

git branch -d nombre_rama
eliminaar rama local

git push origin --delete nombre_rama
eliminar rama en remoto







